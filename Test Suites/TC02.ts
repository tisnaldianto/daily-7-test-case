<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TC02</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>7d03b423-15c6-48c0-88b7-70966aa776ce</testSuiteGuid>
   <testCaseLink>
      <guid>7fec0ead-f8b1-47bf-b3a0-3b57a3d366d8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Positive/S01/TC02 - Upload File Data Driven Method</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>1d9d2bd6-a6a5-46b5-b301-391cd86f3c88</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/TestData</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>1d9d2bd6-a6a5-46b5-b301-391cd86f3c88</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Files to Upload 2</value>
         <variableId>50dc1024-2ec7-4062-8deb-bc45d61a16ea</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
